package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func SetConnection() *sql.DB {

	connectionDB, err := sql.Open("mysql", "root@/apigo")
	if err != nil {
		fmt.Println("Connection Error", err)
	}
	return connectionDB
	
}
