package routes

import (
	"apitasks/database"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"github.com/gorilla/mux"
)

type Task struct {
	ID          int    `json:"id"`
	description string `json:"descricao"`
}

var tasks []Task

func NovaTask(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		w.WriteHeader(http.StatusCreated)
		conn := database.SetConnection()
		defer conn.Close()
		var register Task
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Fprint(w, "Bad Request")
		}
		json.Unmarshal(body, &register)
		action, err := conn.Prepare("insert into task (description) values(?)")
		action.Exec(register.description)
		encoder := json.NewEncoder(w)
		encoder.Encode(register)

	}
}

func GetTasks(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		conn := database.SetConnection()
		defer conn.Close()
		selDB, err := conn.Query("select * from task")
		if err != nil {
			fmt.Println("Error to fetch", err)
		}
		for selDB.Next() {
			var task Task
			err = selDB.Scan(&task.ID, &task.description)
			tasks = append(tasks, task)
		}
		encoder := json.NewEncoder(w)
		encoder.Encode(tasks)

	}
}

func GetTaskById(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		conn := database.SetConnection()
		defer conn.Close()
		var task Task
		vars := mux.Vars(r)
		id := vars["id"]
		selDB := conn.QueryRow("select * from task where id=" + id)
		selDB.Scan(&task.ID, &task.description)
		encoder := json.NewEncoder(w)
		encoder.Encode(task)

	}
}

func UpdateTaskById(w http.ResponseWriter, r *http.Request) {

	if r.Method == "PUT" {
		w.WriteHeader(http.StatusCreated)
		conn := database.SetConnection()
		defer conn.Close()
		var register Task
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Fprint(w, "Bad Request")
		}
		vars := mux.Vars(r)
		id := vars["id"]
		json.Unmarshal(body, &register)
		action, err := conn.Prepare("update task set description=? where id=" + id)
		action.Exec(register.description)
		encoder := json.NewEncoder(w)
		encoder.Encode(register)

	}
}

func DeletarTaskById(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE" {
		conn := database.SetConnection()
		defer conn.Close()
		vars := mux.Vars(r)
		id := vars["id"]
		_, err := conn.Exec("delete from task where id=" + id)
		if err != nil {
			fmt.Fprint(w, "Error on delete")
		} else {
			fmt.Fprint(w, "Deleted with successfully")
		}

	}
}
