package main

import (
	"apitasks/middlewares"
	"apitasks/routes"
	"fmt"
	"net/http"
	"github.com/gorilla/mux"
)

func majorRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "!!! WELCOME !!!")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", majorRoute)
	router.HandleFunc("/tasks", routes.GetTasks)
	router.HandleFunc("/novaTasks", routes.NovaTask)
	router.HandleFunc("/tasks/{id}", routes.GetTaskById)
	router.HandleFunc("/updateTask/{id}", routes.UpdateTaskById)
	router.HandleFunc("/deletarTask/{id}", routes.DeletarTaskById)
}

func main() {

	var router *mux.Router

	router = mux.NewRouter()

	fmt.Println("Servidor funcionando")

	router.Use(middlewares.JsonMiddleware)

	setRoutes(router)

	err := http.ListenAndServe(":33", router)

	if err != nil {
		fmt.Println("Error", err)
	}

}
